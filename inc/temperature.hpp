#ifndef TEMPERATURE_H
#define TEMPERATURE_H
#include <iostream>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include "bme280.hpp"


class TemperatureSensorIn{
	private:
		double temp;
	public:
		TemperatureSensorIn();
		void updateTemperature();
		double getTemperature();
};
class TemperatureSensorOut{
	private:
	struct bme280_dev dev;
    struct identifier id;
	double temp, press, hum;
	public:
		TemperatureSensorOut();
		void updateData();
		double getTemperature();
		double getPressure();
		double getHumidity();
		void init();
};

#endif
