#ifndef GPIO_H
#define GPIO_H

class Fan{
    private:
        bool state;
    public:
        Fan();
        void turnOn();
        void turnOff();
        bool getState();
};

class Resistor{
    private:
        bool state;
    public:
        Resistor();
        void turnOn();
        void turnOff();
        bool getState();
};
#endif