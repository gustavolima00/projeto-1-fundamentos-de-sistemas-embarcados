#ifndef LCD_H
#define LCD_H

#include <string>
#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
// Define some device parameters
#define I2C_ADDR   0x27 // I2C device address

// Define some device constants
#define LCD_CHR  1 // Mode - Sending data
#define LCD_CMD  0 // Mode - Sending command

#define LINE1  0x80 // 1st line
#define LINE2  0xC0 // 2nd line

#define LCD_BACKLIGHT   0x08  // On
// LCD_BACKLIGHT = 0x00  # Off

#define ENABLE  0b00000100 // Enable bit

class DisplayLCD{
	private:
		int fd;
		void lcdLoc(int line);
		void lcd_byte(int bits, int mode);
		void toggle(int bits);
	public:
		DisplayLCD();
		void type(const char* s);
		void type(const char* s, int line);
		void clear();
};

#endif
