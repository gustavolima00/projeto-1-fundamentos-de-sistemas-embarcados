#ifndef INTERFACE_H
#define INTERFACE_H
#include <curses.h>
#include <string>
using namespace std;


#define INFO_X_START 0
#define INFO_Y_START 60
#define INFO_HEIGHT 14
#define INFO_WIDTH 60

#define MENU_X_START 0
#define MENU_Y_START 0
#define MENU_HEIGHT 11
#define MENU_WIDTH 60

#define INPUT_X_START MENU_HEIGHT
#define INPUT_Y_START 0
#define INPUT_HEIGHT 3
#define INPUT_WIDTH 60

#define MAIN_MENU 0
#define CHOSE_TEMPERATURE 1
#define UPDATE_TEMP_FEEDBACK 2
#define UPDATE_POT_FEEDBACK 3
#define CHOSE_HYSTERESIS 4
#define UPDATE_HYST_FEEDBACK 5

#define USER_REFER 1
#define POTENCIOMETER_REFER 2
#define UART_DELAY 10000

#define COLOR_SELECTED 2
#define COLOR_UNSELECTED 1

#define OPTION_1 0
#define OPTION_2 1
#define OPTION_3 2
#define OPTION_4 3

class Info{
    private:
        double inner_temperature;
        double out_temperature;
        double refer_temperature;
        bool fan_state;
        bool resistor_state;
        int refer_type;
        double hysteresis;
        void updateReferTemperature();
        WINDOW *window;
    public:
        Info();
        Info(double inner_temperature, double out_temperature, double refer_temperature, bool fan_state, bool resistor_state);
        void setInnerTemperature(double inner_temperature);
        void setOutTemperature(double out_temperature);
        void setReferTemperature(double refer_temperature);
        void setFanState(bool fan_state);
        void setResistorState(double resistor_state);
        void updatePotenciometerRefer();
        void updateScreen();
        void init();
        double getReferTemperature();
        void setHysteresis(double hysteresis);
        double getHysteresis();
};
class Input{
    private:
        void updateScreen();
        bool isActive;
        WINDOW *window;
    public:
        Input();
        double getDouble();
        int getInt();
        void deactivate();
        void activate();
};
class Menu{
    private:
        void updateScreen();
        int key_input;
        WINDOW *window;
        Input input_screen;
        int curr_screen;
        double refer_temperature;
        bool running;
        bool potenciometer_refer;
        double hysteresis;
        int current_option;
    public:
        Menu();
        void menuHandler();
        bool isRunning();
        double getReferTemperature();
        bool PotenciometerIsRefer();
        void init();
        double getHysteresis();
};

void showEndScreen();

void InterfaceInit();

#endif
