#ifndef LOG_H
#define LOG_H

void initLog();

void registerLog(double inner_temperature, double extern_temperature, double refer_temperature);

#endif