#ifndef UART_H
#define UART_H
#include <cstdint> 

int get_fstream();

int send_code(uint8_t code);

float get_value(int uart0_filestream);

#endif