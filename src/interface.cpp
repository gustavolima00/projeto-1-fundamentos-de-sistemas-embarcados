#include <iostream>
#include <iomanip>
#include "interface.hpp"
#include "uart.hpp"
#include <unistd.h>
#include <curses.h>
#include <fstream>
#include <string>
#include "uart.hpp"

using namespace std;

Info::Info(){
    
}   

void Info::init(){
    this->window = newwin(INFO_HEIGHT, INFO_WIDTH, INFO_X_START, INFO_Y_START);
    keypad(this->window, FALSE); 
    box(this->window , 0, 0);
    this->refer_type = POTENCIOMETER_REFER;
    this->hysteresis = 4.0;
    this->updateScreen();
}

Info::Info(double inner_temperature, double out_temperature, double refer_temperature, bool fan_state, bool resistor_state){
    this->inner_temperature = inner_temperature;
    this->out_temperature = out_temperature;
    this->refer_temperature = refer_temperature;
    this->fan_state = fan_state;
    this->resistor_state = resistor_state;
    this->window = newwin(INFO_HEIGHT, INFO_WIDTH, INFO_X_START, INFO_Y_START);
    box(this->window , 0, 0);
    this->updateScreen();
}

void Info::updateScreen(){
    wclear(this->window);
    if(this->refer_type == POTENCIOMETER_REFER){
        this->updateReferTemperature();
    }
    box(this->window , 0, 0);
    mvwprintw(this->window, 1, 2, "Informacoes do sistema");
	mvwprintw(this->window, 3, 2, "Temperatura de referencia : %.2lf ºC", this->refer_temperature);
	mvwprintw(this->window, 4, 2, "Temperatura interna       : %.2lf ºC", this->inner_temperature);
	mvwprintw(this->window, 5, 2, "Temperatura externa       : %.2lf ºC", this->out_temperature);
	if(this->fan_state)
		mvwprintw(this->window, 7, 2, "Ventoinhana ligada");
	else
		mvwprintw(this->window, 7, 2, "Ventoinhana desligada");
	if(this->resistor_state)
		mvwprintw(this->window, 8, 2, "Resistor ligado");
	else
		mvwprintw(this->window, 8, 2, "Resistor desligado");
    if(this->refer_type == POTENCIOMETER_REFER){
        mvwprintw(this->window, 10, 2, "Tipo de temperatura de referencia: Potenciometro");
    }
    else if(this->refer_type == USER_REFER){
        mvwprintw(this->window, 10, 2, "Tipo de temperatura de referencia: Usuario");
    }
    mvwprintw(this->window, 11, 2, "Histerese: %.2lf ºC", this->hysteresis);
	wrefresh(this->window);
}

void Info::setInnerTemperature(double inner_temperature){
    this->inner_temperature = inner_temperature;
}

void Info::setOutTemperature(double out_temperature){
    this->out_temperature = out_temperature;
}

void Info::setReferTemperature(double refer_temperature){
    this->refer_temperature = refer_temperature;
    this->refer_type = USER_REFER;
}

void Info::setFanState(bool fan_state){
    this->fan_state = fan_state;
}

void Info::setResistorState(double resistor_state){
    this->resistor_state = resistor_state;
}

void Info::updatePotenciometerRefer(){
    this->refer_type = POTENCIOMETER_REFER;
}

void Info::updateReferTemperature(){
    int uart0_filestream = send_code(0xA2);
    usleep(UART_DELAY);
    double res = get_value(uart0_filestream);
    if(res > 0){ 
        this->refer_temperature = res;
    }
}

double Info::getReferTemperature(){
    return this->refer_temperature; 
}

void Info::setHysteresis(double hysteresis){
    this->hysteresis = hysteresis;
}

double Info::getHysteresis(){
    return this->hysteresis;
}

Input::Input(){
    this->window = newwin(INPUT_HEIGHT, INPUT_WIDTH, INPUT_X_START, INPUT_Y_START);
    this->isActive = false;
    this->updateScreen();
}

void Input::updateScreen(){
    wclear(this->window);
    if(this->isActive){
        box(this->window , 0, 0);
        mvwprintw(this->window, 1, 2, "> ");
    }
    wrefresh(this->window);
}

void Input::activate(){
    this->isActive = true;
    this->updateScreen();
}

void Input::deactivate(){
    this->isActive = false;
    this->updateScreen();
}

double Input::getDouble(){
    if(!this->isActive) return -1;
    double value;
	wscanw(this->window, "%lf", &value);
    this->updateScreen();
    return value;
}
int Input::getInt(){
    if(!this->isActive) return -1;
    int value;
	wscanw(this->window, "%d", &value);
    this->updateScreen();
    return value;
}

Menu::Menu(){
    this->current_option = 0;
}

void Menu::init(){
    this->window = newwin(MENU_HEIGHT, MENU_WIDTH, MENU_X_START, MENU_Y_START);
    keypad(this->window, TRUE); 
    this->curr_screen = MAIN_MENU;
    this->input_screen = Input();
    this->input_screen.deactivate();
    this->running =  true;
    this->potenciometer_refer = true;
    this->current_option = 0;
    this->updateScreen();
}

void Menu::updateScreen(){
    wclear(this->window);
    box(this->window , 0, 0);
    if(this->curr_screen == MAIN_MENU){
        
        mvwprintw(this->window, 1, 20, "Menu de acoes");
        if(this->current_option == 0){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4, 2, "* Entrar com uma temperatura de referencia");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 4, 2, "  Entrar com uma temperatura de referencia");
        }
        
        if(this->current_option == 1){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 5, 2, "* Usar potenciometro como temperatura de referencia");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 5, 2, "  Usar potenciometro como temperatura de referencia");
        }

        if(this->current_option == 2){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 6, 2, "* Definir valor de histerese");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 6, 2, "  Definir valor de histerese");
        }
        
        if(this->current_option == 3){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 7, 2, "* Sair");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 7, 2, "  Sair");
        }
    }
    else if(this->curr_screen == CHOSE_TEMPERATURE){
        mvwprintw(this->window, MENU_HEIGHT/2-1, MENU_WIDTH/2 - 10, "Entre com o valor da");
        mvwprintw(this->window, MENU_HEIGHT/2, MENU_WIDTH/2 - 13, "temperatura e aperte enter");
    }
    else if(this->curr_screen == CHOSE_HYSTERESIS){
        
        mvwprintw(this->window, MENU_HEIGHT/2-1, MENU_WIDTH/2 - 10, "Entre com o valor de");
        mvwprintw(this->window, MENU_HEIGHT/2, MENU_WIDTH/2 - 12, "histerese e aperte enter");
    }
    else if(this->curr_screen == UPDATE_TEMP_FEEDBACK){
        mvwprintw(this->window, MENU_HEIGHT/2-1, MENU_WIDTH/2 - 21, "A temperatura de refefencia foi atualizada");
        mvwprintw(this->window, MENU_HEIGHT/2, MENU_WIDTH/2 - 7, "para %.2lf ºC", this->refer_temperature);
    }
    else if(this->curr_screen == UPDATE_POT_FEEDBACK){
        mvwprintw(this->window, MENU_HEIGHT/2-1, MENU_WIDTH/2 - 14, "A temperatura de referencia");
        mvwprintw(this->window, MENU_HEIGHT/2, MENU_WIDTH/2 - 17, "agora usa o valor do potenciometro");
    }
    else if(this->curr_screen == UPDATE_HYST_FEEDBACK){
        mvwprintw(this->window, MENU_HEIGHT/2-1, MENU_WIDTH/2 - 17, "O valor de histerese foi atualizado");
        mvwprintw(this->window, MENU_HEIGHT/2, MENU_WIDTH/2 - 7, "para %.2lf ºC", this->hysteresis);
    }
    wrefresh(this->window);
}

void Menu::menuHandler(){
    if(curr_screen == MAIN_MENU){
        int key = wgetch(this->window);
        if(key == KEY_DOWN){
            this->current_option += 1;
            this->current_option %= 4;
        }
        if(key == KEY_UP){
            this->current_option += 4-1;
            this->current_option %= 4;
        }
        if(key == KEY_RIGHT or  key == '\n' or key == KEY_ENTER){
            if(this->current_option == 0){
                this->curr_screen = CHOSE_TEMPERATURE;
            }
            else if(this->current_option == 1){
                this->potenciometer_refer = true;
                this->curr_screen = UPDATE_POT_FEEDBACK;
            }
            else if(this->current_option == 2){
                this->curr_screen = CHOSE_HYSTERESIS;
            }
            else if(this->current_option == 3){
                this->running =  false;
            }
        }

    }
    else if(curr_screen == CHOSE_TEMPERATURE){
        this->input_screen.activate();
        double temp = this->input_screen.getDouble();
        this->refer_temperature = temp;
        this->potenciometer_refer = false;
        this->curr_screen = UPDATE_TEMP_FEEDBACK;
    }
    else if(curr_screen == CHOSE_HYSTERESIS){
        this->input_screen.activate();
        double hysteresis = this->input_screen.getDouble();
        this->hysteresis = hysteresis;
        this->curr_screen = UPDATE_HYST_FEEDBACK;
    }
    else if(curr_screen == UPDATE_TEMP_FEEDBACK or curr_screen == UPDATE_POT_FEEDBACK or curr_screen == UPDATE_HYST_FEEDBACK){
        this->input_screen.deactivate();
        sleep(2);
        this->curr_screen = MAIN_MENU;
    }
    this->updateScreen();
}

double Menu::getHysteresis(){
    return this->hysteresis;
}

double Menu::getReferTemperature(){
    return this->refer_temperature;
}
bool Menu::isRunning(){
    return this->running;
}

bool Menu::PotenciometerIsRefer(){
    return this->potenciometer_refer;
}

void showEndScreen(){
    int row, col;
    getmaxyx(stdscr,row,col);
    WINDOW *window = newwin(row, col, 0, 0);
    wclear(window);
    box(window , 0, 0);
    mvwprintw(window, row/2, col/2-15, "Programa encerrado com sucesso");
    mvwprintw(window, row/2+1, col/2-11, "Aperte enter para sair");
    wrefresh(window);
    getch();
    clear();
    refresh();
    endwin();
}

void InterfaceInit(){
    initscr();
    if(has_colors()){
        start_color();
    }
    else{

    }
	curs_set(0);
	echo();
	cbreak();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_BLACK, COLOR_WHITE);
    

	refresh();
    int row, col;
    getmaxyx(stdscr,row,col);
    WINDOW *window = newwin(row, col, 0, 0);
    wclear(window);
    box(window , 0, 0);
    mvwprintw(window, row/2, col/2-30, "Para uma melhor experiencia aumente o tamanho do seu terminal");
    mvwprintw(window, row/2+1, col/2-16, "Aperte uma tecla para para continuar");
    refresh();
    wrefresh(window);
    getch();
    wclear(window);
    wrefresh(window);
}
