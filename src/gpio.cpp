#include <bcm2835.h>
#include "gpio.hpp"
#include <iostream>
using namespace std;

Fan::Fan(){
    if (!bcm2835_init()){
        cerr << "Erro ao iniciar bcm2835\n";
        exit(1);
    };
    bcm2835_gpio_fsel(RPI_V2_GPIO_P1_18, BCM2835_GPIO_FSEL_OUTP);
    this->turnOff();
}

void Fan::turnOn(){
    bcm2835_gpio_write(RPI_V2_GPIO_P1_18, 0);
    this->state = true;
}

void Fan::turnOff(){
    bcm2835_gpio_write(RPI_V2_GPIO_P1_18, 1);
    this->state = false;
}

bool Fan::getState(){
    return this->state;
}
Resistor::Resistor(){
    if (!bcm2835_init()){
        cerr << "Erro ao iniciar bcm2835\n";
        exit(1);
    };
    bcm2835_gpio_fsel(RPI_V2_GPIO_P1_16, BCM2835_GPIO_FSEL_OUTP);
    this->turnOff();
}

void Resistor::turnOn(){
    bcm2835_gpio_write(RPI_V2_GPIO_P1_16, 0);
    this->state = true;
}

void Resistor::turnOff(){
    bcm2835_gpio_write(RPI_V2_GPIO_P1_16, 1);
    this->state = false;
}
bool Resistor::getState(){
    return this->state;
}