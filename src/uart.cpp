
#include "uart.hpp"
#include <stdio.h>
#include <string.h>
#include <unistd.h> //Usado para a UART
#include <fcntl.h> //Usado para a UART
#include <termios.h> //Usado para a UART
#include <stdlib.h>
#include <iostream>
using namespace std;
const char file_path[] = "/dev/serial0";
const unsigned char mt[] = {5, 1, 5, 8};

int get_fstream(){
	int f_stream = -1;
	f_stream = open(file_path, O_RDWR | O_NOCTTY |O_NDELAY); 
	if(f_stream == -1){
		printf("Erro - Porta Serial nao pode ser aberta. Confirme se não está sendo usada por outra aplicação.\n");
		return -1;
	}
	struct termios options;
	tcgetattr(f_stream, &options);
	options.c_cflag = B115200 | CS8 | CLOCAL | CREAD; // Set baud rate
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(f_stream, TCIFLUSH);
	tcsetattr(f_stream, TCSANOW, &options);
	return f_stream;
}

int send_code(uint8_t code){
    unsigned char p_tx_buffer[5];
    unsigned char *p  = p_tx_buffer;
    *p++ = code;
    *p++ = mt[0];
    *p++ = mt[1];
    *p++ = mt[2];
    *p++ = mt[3];

    int uart0_filestream = get_fstream();
    if (uart0_filestream != -1)
    {
        int count = write(uart0_filestream, p_tx_buffer, 5 * sizeof(unsigned char));
        if (count < 0){
            cerr << "UART TX error\n";
            exit(1);
        }
    }
    return uart0_filestream;    
}

float get_value(int uart0_filestream){
    float value = -1.0;
	if (uart0_filestream != -1){
		read(uart0_filestream, &value, sizeof(value));
        close(uart0_filestream);
	}
    return value;
}