#include "lcd.hpp"
#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>

void DisplayLCD::lcd_byte(int bits, int mode)
{
	//Send byte to data pins
	// bits = the data
	// mode = 1 for data, 0 for command
	int bits_high;
	int bits_low;
	// uses the two half byte writes to LCD
	bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT;
	bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT;

	// High bits
	wiringPiI2CReadReg8(fd, bits_high);
	this->toggle(bits_high);

	// Low bits
	wiringPiI2CReadReg8(fd, bits_low);
	this->toggle(bits_low);
}

void DisplayLCD::lcdLoc(int line)
{
	this->lcd_byte(line, LCD_CMD);
}

DisplayLCD::DisplayLCD()
{
	if (wiringPiSetup() == -1)
		exit(1);

	this->fd = wiringPiI2CSetup(I2C_ADDR);
	this->lcd_byte(0x33, LCD_CMD); // Initialise
	this->lcd_byte(0x32, LCD_CMD); // Initialise
	this->lcd_byte(0x06, LCD_CMD); // Cursor move direction
	this->lcd_byte(0x0C, LCD_CMD); // 0x0F On, Blink Off
	this->lcd_byte(0x28, LCD_CMD); // Data length, number of lines, font size
	this->lcd_byte(0x01, LCD_CMD); // Clear display
}


void DisplayLCD::type(const char *s, int line)
{
	lcdLoc(line);
	while(*s)
		this->lcd_byte(*s++, LCD_CHR);
}
void DisplayLCD::type(const char *s)
{
	string line_1, line_2;
	while(*s!='\0' and *s!='\n')
		line_1.push_back(*s++);
	if(*s=='\n') ++s;
	while(*s)
		line_2.push_back(*s++);
	this->type(line_1.c_str(), LINE1);
	this->type(line_2.c_str(), LINE2);
}
void DisplayLCD::clear()
{
	this->lcd_byte(0x01, LCD_CMD);
	this->lcd_byte(0x02, LCD_CMD);
}

void DisplayLCD::toggle(int bits)
{
	// Toggle enable pin on LCD display
	delayMicroseconds(500);
	wiringPiI2CReadReg8(fd, (bits | ENABLE));
	delayMicroseconds(500);
	wiringPiI2CReadReg8(fd, (bits & ~ENABLE));
	delayMicroseconds(500);
}
