#include <iostream>
#include <iomanip>
#include <curses.h>
#include <string>
#include <thread>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <math.h>
#include "temperature.hpp"
#include "lcd.hpp"
#include "gpio.hpp"
#include "uart.hpp"
#include "log.hpp"
#include "interface.hpp"
using namespace std;

/* Variáveis das threads */
thread info_thread, menu_thread;
bool menu_thread_start = false;
bool menu_thread_finish = false;
bool info_thread_start = false;
bool info_thread_finish = false;
short int count = 0;

/* Telas */
Info info_screen;
Menu menu_screen;

/* Componentes */
TemperatureSensorIn t_in;
TemperatureSensorOut t_out;
DisplayLCD lcd;
Fan fan;
Resistor resistor;

void updateInfo();
void sig_handler(int signum);
void menuHandler();

int main()
{
	signal(SIGALRM, sig_handler);
	t_out.init();
	initLog();
	InterfaceInit();
	info_screen.init();
	menu_screen.init();
	ualarm(100000, 100000);
	while (1)
		pause();
}

void sig_handler(int signum)
{
	count = (count + 1) % 20;

	/* Fim do programa */
	if (!menu_screen.isRunning())
	{
		showEndScreen();
		exit(1);
	}

	/* Atualização do menu/input (100 ms) */
	if (not menu_thread_start)
	{
		menu_thread_start = true;
		menu_thread_finish = false;
		menu_thread = thread(menuHandler);
	}

	/* Atualização das informações (500 ms) */
	if (count % 5 == 0 and not info_thread_start)
	{
		info_thread_start = true;
		info_thread_finish = false;
		info_thread = thread(updateInfo);
	}

	/* Registro do log (2000 ms) */
	if (count % 20 == 0)
	{
		registerLog(t_in.getTemperature(), t_out.getTemperature(), info_screen.getReferTemperature());
	}

	/* Join da thread do menu/input */
	if (menu_thread_start and menu_thread_finish)
	{
		menu_thread_start = false;
		menu_thread.join();
	}

	/* Join da thread de atualização das informações */
	if (info_thread_start and info_thread_finish)
	{
		info_thread_start = false;
		info_thread.join();
	}
}
void updateInfo()
{
	/* Atualização das informações dos componentes */
	t_in.updateTemperature();
	t_out.updateData();
	if (menu_screen.PotenciometerIsRefer())
		info_screen.updatePotenciometerRefer();
	else
		info_screen.setReferTemperature(menu_screen.getReferTemperature());
	double tr = info_screen.getReferTemperature();
	double ti = t_in.getTemperature();
	double te = t_out.getTemperature();
	double hys = menu_screen.getHysteresis();

	/* Controle da temperatura */
	if (fabs(ti - tr) < hys / 2) // Dentro do valor de histerese
	{ 
		resistor.turnOff();
		fan.turnOff();
	}
	else if ((ti - tr) < 0) // TI < TR
	{ 
		resistor.turnOn();
		fan.turnOff();
	}
	else // TI > TR
	{ 
		resistor.turnOff();
		fan.turnOn();
	}

	/* Preenchimento das informações no LCD */
	char visor_str[50];
	sprintf(visor_str, "TI:%.1lf TE:%.1lf\nTR:%.1lf", ti, te, tr);
	lcd.type(visor_str);

	/* Atualização das informações na tela */
	info_screen.setHysteresis(hys);
	info_screen.setInnerTemperature(ti);
	info_screen.setOutTemperature(te);
	info_screen.setFanState(fan.getState());
	info_screen.setResistorState(resistor.getState());
	info_screen.updateScreen();
	info_thread_finish = true;
}
void menuHandler()
{
	menu_screen.menuHandler();
	menu_thread_finish = true;
}
