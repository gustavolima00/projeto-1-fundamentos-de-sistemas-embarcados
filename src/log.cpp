#include <iostream>
#include <fstream>
#include "log.hpp"
#include <chrono>
#include <iomanip>
#include <ctime>
using namespace std;

void initLog()
{
    time_t now = time(0);
    tm *time_info = localtime(&now);
    string file_name = "log_" + to_string(time_info->tm_mday) + "-" + to_string(1 + time_info->tm_mon) + "-" + to_string(1900 + time_info->tm_year) + "_" + to_string(time_info->tm_hour) + ":" + to_string(time_info->tm_min) + ".csv";
    ofstream file;
    file.open(file_name);
    file << "Data (DD/MM/YYYY), Horario (HH:MM:SS), Temperatura Interna (ºC), Temperatura externa (ºC), Temperatura de referência (ºC)\n";
    file.close();
}
void registerLog(double inner_temperature, double extern_temperature, double refer_temperature)
{
    time_t now = time(0);
    tm *time_info = localtime(&now);
    ofstream file;
    file.open("log.csv", ios::app);
    file << fixed << setprecision(3);
    file << time_info->tm_mday << "/" << 1 + time_info->tm_mon << "/" << 1900 + time_info->tm_year << ", ";
    file << time_info->tm_hour << ":" << time_info->tm_min << ":" << time_info->tm_sec << ", ";
    file << inner_temperature << ", ";
    file << extern_temperature << ", ";
    file << refer_temperature << "\n";
    file.close();
}
