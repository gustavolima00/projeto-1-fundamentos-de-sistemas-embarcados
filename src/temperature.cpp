#include "temperature.hpp"
#include <iostream>
#include <string>
#include "uart.hpp"
#include <unistd.h>
using namespace std;

const string i2c_bus = "/dev/i2c-1";

TemperatureSensorIn::TemperatureSensorIn(){
    updateTemperature();
}

double TemperatureSensorIn::getTemperature(){
    return this->temp;
}
void TemperatureSensorIn::updateTemperature(){
    int uart0_filestream = send_code(0xA1);
    usleep(10000);
    double res = get_value(uart0_filestream);
    if(res > 0){ 
        this->temp = res;
    }
}
TemperatureSensorOut::TemperatureSensorOut(){

}
void TemperatureSensorOut::init(){
	if ((this->id.fd = open(i2c_bus.c_str(), O_RDWR)) < 0)
    {
        cerr << "Failed to open the i2c bus\n";
        exit(1);
    }
	this->id.dev_addr = BME280_I2C_ADDR_PRIM;
    if (ioctl(this->id.fd, I2C_SLAVE, this->id.dev_addr) < 0)
    {
		cerr << "Failed to acquire bus access and/or talk to slave.\n";
        exit(1);
    }
	this->dev.intf = BME280_I2C_INTF;
    this->dev.read = user_i2c_read;
    this->dev.write = user_i2c_write;
    this->dev.delay_us = user_delay_us;
	this->dev.intf_ptr = &id;
    
    int8_t rslt = BME280_OK;
	rslt = bme280_init(&this->dev);
    if (rslt != BME280_OK)
    {
		cerr << "Failed to initialize the device (code " << rslt << ").\n";
        exit(1);
    }
    updateData();
}

void TemperatureSensorOut::updateData(){
	int8_t rslt = BME280_OK;
    rslt = stream_sensor_data_forced_mode(&this->dev, this->temp, this->press, this->hum);
    if (rslt != BME280_OK)
    {
		cerr << "Failed to stream sensor data (code " << rslt << ").\n";
        exit(1);
    }
}

double TemperatureSensorOut::getTemperature(){
	return this->temp;
}
double TemperatureSensorOut::getPressure(){
	return this->press;
}
double TemperatureSensorOut::getHumidity(){
	return this->hum;
}
