# Fundamentos de Sistemas Embarcados - Projeto 1

## Como rodar

Para compilar o projeto basta executar o comando 
- make

Para executar o projeto basta executar o comando
- make run

Para limpar objetos compilados execute o comando
- make clean

## Dentro do programa

Para uma melhor experiência é recomendado aumentar a tela do terminal

### Menu de opções

O menu de opções é controlado pelas setas do teclado, para escolher uma opção basta teclar ENTER

#### Imagens

![](https://i.imgur.com/31iZ419.png)

![](https://i.imgur.com/uYthuGA.png)

#### informações
No menu a esquerda é possível acompanhar as informações do Sistemas

## informações técnicas

### Adaptação das bibliotecas
O programa foi desenvolvido em C++ e possui algumas adaptações das bibliotecas originalmente em C

### Ncurses
A biblioteca ultilizada para criação do menu foi o ncurses e ela pode apresentar algumas falhas comprometendo a visualização do terminal. Caso aconteca algo similar é recomendado fechar o terminal e rodar o programa novamente.

### Possíveis falhas na inicialização do sensor de temperatura
Caso o sensor de temperatura apresente falha ao iniciar é recomendado rodar o projeto novamente. Caso o problema persista basta limpar os dados compilados com **make clean** e recompilar com **make** 